SystemName      Strontium titanate (SrTiO3) cubic structure
#               Cubic phase
#               LDA functional, Ceperley-Alder parametrization
#               Theoretical lattice constant 
#               Mesh Cutoff: 600 Ry
#               Monkhorst-Pack grid: 6 x 6 x 6 ; displaced 0.5 0.5 0.5 

SystemLabel	SrTiO3
NumberOfSpecies	3
NumberOfAtoms	5
%block ChemicalSpeciesLabel
      1      38     Sr
      2      22     Ti
      3       8     O
%endblock ChemicalSpeciesLabel

%block PS.lmax
   Sr    3
   Ti    3
    O    3
%endblock PS.lmax

%block PAO.Basis
Sr   5      1.64000
 n=4   0   1   E   155.00000     6.00000
     6.49993
     1.00000
 n=5   0   2   E   149.48000     6.50000
     6.99958     5.49957
     1.00000     1.00000
 n=4   1   1   E   148.98000     5.61000
     6.74964
     1.00000
 n=5   1   1   E     4.57000     1.20000
     4.00000
     1.00000
 n=4   2   1   E   146.26000     6.09000
     6.63062
     1.00000
Ti    5      1.91
 n=3    0    1   E     93.95      5.20
   5.69946662616249
   1.00000000000000
 n=3    1    1   E     95.47      5.20
   5.69941339465994
   1.00000000000000
 n=4    0    2   E     96.47      5.60
   6.09996398975307        5.09944363262274
   1.00000000000000        1.00000000000000
 n=3    2    2   E     46.05      4.95
   5.94327035784617        4.70009988294302
   1.00000000000000        1.00000000000000
 n=4    1    1   E      0.50      1.77
   3.05365979938936
   1.00000000000000
O     3     -0.28
 n=2    0    2   E     40.58      3.95
   4.95272270428712        3.60331408800389
   1.00000000000000        1.00000000000000
 n=2    1    2   E     36.78      4.35
   4.99990228025066        3.89745395068600
   1.00000000000000        1.00000000000000
 n=3    2    1   E     21.69      0.93
   2.73276990670788
   1.00000000000000
%endblock PAO.Basis


LatticeConstant       3.874 Ang       # Theoretical lattice constant for the
                                      #    cubic phase  
%block LatticeVectors
  1.00  0.00  0.00
  0.00  1.00  0.00
  0.00  0.00  1.00
%endblock LatticeVectors

AtomicCoordinatesFormat	     Fractional
%block AtomicCoordinatesAndAtomicSpecies
  0.00  0.00  0.00       1      87.62       Sr
  0.50  0.50  0.50       2      47.867      Ti
  0.00  0.50  0.50       3      15.9994     O
  0.50  0.00  0.50       3      15.9994     O
  0.50  0.50  0.00       3      15.9994     O
%endblock AtomicCoordinatesAndAtomicSpecies

WriteCoorStep           .true.        #  Write the atomic coordinates to 
                                      #     standard output at every 
                                      #     MD time step or relaxation step.

%block kgrid_Monkhorst_Pack
   6  0  0  0.5
   0  6  0  0.5
   0  0  6  0.5
%endblock kgrid_Monkhorst_Pack

#
# DFT, Grid, SCF
#

XC.Functional          LDA
XC.Authors             CA 

MeshCutoff             600 Ry      # Defines the plane wave cutoff for the grid
DM.NumberPulay         3           # It controls the Pulay convergence 
                                   #   accelerator.
DM.UseSaveDM           .true.      # Use the Density Matrix from the DM file 
                                   #   if found
DM.Tolerance           1.d-4       # Tolerance in maximum difference
                                   # between input and output DM
MaxSCFIterations       100         # Maximum number of SCF Iterations
ElectronicTemperature  0.075 eV    # Electronic Temperature for the smearing
                                   #   of the Fermi-Dirac occupation function
SCF.MixAfterConvergence .false.    # Logical variable to indicate whether mixing
                                   #   is done in the last SCF cycle 
                                   #   (after convergence has been achieved) 
                                   #   or not. 
                                   #   Not mixing after convergence improves 
                                   #   the quality of the final Kohn-Sham 
                                   #   energy and of the forces when mixing 
                                   #   the DM.

#
# Molecular Dynamic or Relaxation variables
#

MD.TypeOfRun            cg          # Type of dynamics:
                                    #   - CG
                                    #   - Verlet
                                    #   - Nose
                                    #   - Parrinello-Rahman
                                    #   - Nose-Parrinello-Rahman
                                    #   - Anneal
                                    #   - FC
MD.VariableCell         .true.      # The lattice is relaxed together with
                                    # the atomic coordinates?
MD.NumCGsteps            0          # Number of CG steps for
                                    #   coordinate optimization
MD.MaxCGDispl           0.3 Bohr    # Maximum atomic displacement
                                    #   in one CG step
MD.MaxForceTol         0.01 eV/Ang  # Tolerance in the maximum
                                    #   atomic force
MD.MaxStressTol        0.0001 eV/Ang**3  
                                    # Tolerance in the maximum
                                    #   stress in a MD.VariableCell CG optimi.

MD.UseSaveXV           .false.      # Instructs Siesta to read the 
                                    #   atomic positions and velocities stored
                                    #   in file SystemLabel.XV by a 
                                    #   previous run.
MD.UseSaveCG           .false.      # Instructs to read the conjugate-gradient 
                                    #   hystory information stored in file 
                                    #   SystemLabel.CG by a previous run.

#
# Plotting the band structure
#

BandLinesScale pi/a
%block BandLines
1   0.0 0.0 0.0 \Gamma              # Start at \Gamma
100 1.0 0.0 0.0 X                   # 100 points from \Gamma to X
100 1.0 1.0 0.0 M                   # 100 points from X to M
100 1.0 1.0 1.0 R                   # 100 points from M to R
173 0.0 0.0 0.0 \Gamma              # 173 points from R to \Gamma
141 0.0 1.0 1.0 M                   # 141 points from \Gamma to M
100 0.0 1.0 0.0 X                   # 100 points from M to X
141 1.0 1.0 1.0 R                   # 141 points from X to R
%endblock BandLines

#
# Plotting the Projected Density Of States
#

%block ProjectedDensityOfStates
  -70.00  5.00  0.150 3000  eV
%endblock ProjectedDensityOfStates

%PDOS.kgrid_Monkhorst_Pack
   60  0  0  0.5
    0 60  0  0.5
    0  0 60  0.5
%end PDOS.kgrid_Monkhorst_Pack

#
# Computing the fat bands
#

COOP.Write             .true.       # Instructs the program to generate
                                    #   SystemLabel.fullBZ.WFSX
                                    #   (packed wavefunction file)
                                    #   and SystemLabel.HSX (H, S and X ij file)
                                    #   to be processed by Util/COOP/mprop and
                                    #   Util/COOP/fat and
                                    #   to generate COOP/COHP curves,
                                    #   (projected) densities of states,
                                    #   fat bands, etc.
WFS.Write.For.Bands    .true.       # Instructs the program to compute and
                                    #   write the wave functions associated to 
                                    #   the bands specified
                                    #   (by a BandLines or a BandPoints block)
                                    #   to the file SystemLabel.bands.WFSX.
WFS.band.min           1            # Specifies the lowest band index of the
                                    #   wave-functions to be written to the file
                                    #   (in this context) 
                                    #   SystemLabel.fullBZ.WFSX
                                    #   for each k-point
                                    #   (all k-points in the BZ sampling
                                    #   are affected).
WFS.band.max           72           # Same as before, but for the
                                    #   highest band index

#
# Variables related with the Wannierization of the manifolds
#

%block Wannier.Manifolds
  first
  bottom_cond
  all
%endblock Wannier.Manifolds

%block Wannier.Manifold.first
bands  12  20             # Indices of the initial and final band of the manifold
trial-orbitals  36 37 38 49 50 51 62 63 64  
spreading.nitt 0         # Number of iterations for the minimization of \Omega
wannier_plot  3    # Plot the Wannier function
fermi_surface_plot true # Plot the Fermi surface
write_hr true          # Write the Hamiltonian in the WF basis
write_tb true          # Write lattice vectors, Hamiltonian, and position operator in WF basis
%endblock Wannier.Manifold.first

%block Wannier.Manifold.bottom_cond
bands  21  23             # Indices of the initial and final band of the manifold
trial-orbitals  -1 -2 -3  # Indices of the orbitals that will be used as localized trial orbitals
spreading.nitt 0         # Number of iterations for the minimization of \Omega
wannier_plot  3    # Plot the Wannier function
fermi_surface_plot  true # Plot the Fermi surface
write_hr true          # Write the Hamiltonian in the WF basis
write_tb   true        # Write lattice vectors, Hamiltonian, and position operator in WF basis
%endblock Wannier.Manifold.bottom_cond

 %block Wannier.Projectors.bottom_cond
 0.5 0.5 0.5  2  2  1 0.00  0.00  1.00   1.00  0.00  0.00    1.00
 0.5 0.5 0.5  2  3  1 0.00  0.00  1.00   1.00  0.00  0.00    1.00
 0.5 0.5 0.5  2  5  1 0.00  0.00  1.00   1.00  0.00  0.00    1.00
%endblock Wannier.Projectors.bottom_cond

%block Wannier.Manifold.all
bands  12  23             # Indices of the initial and final band of the manifold
trial-orbitals  24 25 27 36 37 38 49 50 51 62 63 64  # Indices of the orbitals that will be used as localized trial orbitals
spreading.nitt 0         # Number of iterations for the minimization of \Omega
wannier_plot  3    # Plot the Wannier function
fermi_surface_plot true # Plot the Fermi surface
write_hr  true         # Write the Hamiltonian in the WF basis
write_tb  true         # Write lattice vectors, Hamiltonian, and position operator in WF basis
%endblock Wannier.Manifold.all

Wannier.Manifolds.Unk  .false.
Wannier.k [6 6 6]

